### Flyer du concours

# Contexte du projet 
Le concours se déroulera à Carmaux le 12 juin prochain. Les participants devront s'inscrire au minimum un mois avant l'événement en envoyant un message à rouletaboule@cornomail.net et préciser les équipes complètes (2 ou 4 personnes majeures obligatoirement), ainsi que leur club d'affiliation, s'il en sont adhérant.

La charte graphique est assez libre : le projet doit être sur une base de vert et marron avec une police sans empattement. Le noir est interdit pour les textes, et une illustration doit être inclue. Bien entendu, la création doit être absolument magnifique et donner une envie irrésistible de venir à l'événement, qu'on soit joueur ou non :)

L'engagement est fixé à 10€ pour les licenciés, et 15€ pour les autres. La logistique sur place permettra de se restaurer et se désaltérer. Un espace sanitaire sera à disposition. Pas de distributeur de billet sur place.


# Installation 
 npm install parcel -bundler 
 npm init -y
 npx parcel index.html

 sudo npm install bootstratp@next

 # Maquettes 
 Les maquettes sont disponible en pdf dans le dossier "wireframes"

# Lien version en ligne 

http://eval2_flyer.surge.sh/

 git clone git@gitlab.com:amelleouldselma/eval2_flyer.git

 cd 

 # Perfomance 96% pour mobile et 96% ordi
https://developers.google.com/speed/pagespeed/insights/?url=http%3A%2F%2Feval2_flyer.surge.sh%2F

 # Impacte écologique 46
https://www.website-footprint.com/en/result?w=http:%2F%2Feval2_flyer.surge.sh%2F

 # Eco-index A
 http://www.ecoindex.fr/resultats/?id=120493 

 # Rapiditer du site 
 score google pagespeed 96
 temps de chargement 2.9s
https://www.uptrends.fr/outils-gratuits/speed-test-site-web 

# Color Contrast Accessibility Validator
https://color.a11y.com/Contrast/ 

# Web Accessibility 92%
https://www.webaccessibility.com/results/?url=http%3A%2F%2Feval2_flyer.surge.sh%2F 